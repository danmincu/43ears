export function JAudioContext(context) {
    var AudioContext = window.AudioContext || window.webkitAudioContext;
    return new AudioContext();    
};
