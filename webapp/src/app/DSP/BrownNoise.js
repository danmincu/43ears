export function createBrownNoise(context) {
    var bufferSize = 4096;
    var lastOut = 0.0;
    var brownNoise = context.createScriptProcessor(bufferSize, 1, 1);
    brownNoise.onaudioprocess = function (e) {
      var output = e.outputBuffer.getChannelData(0);
      for (var i = 0; i < bufferSize; i++) {
        var white = Math.random() * 2 - 1;
        output[i] = (lastOut + (0.02 * white)) / 1.02;
        lastOut = output[i];
        output[i] *= 3.5; // (roughly) compensate for gain
      }
    }
    return brownNoise;
  };


export function createOceanBrownNoise(context) {
    var bufferSize = 4096;
    var lastOut = 0.0;
    var brownNoise = context.createScriptProcessor(bufferSize, 1, 1);
    brownNoise.onaudioprocess = function (e) {
      var output = e.outputBuffer.getChannelData(0);
      for (var i = 0; i < bufferSize; i++) {
        var white = Math.random() * 2 - 1;
        output[i] = (lastOut + (0.02 * white)) / 1.02;
        lastOut = output[i];
        output[i] *= 3.5; // (roughly) compensate for gain
      }
    }

    var brownGain = context.createGain();
    brownGain.gain.value = 0.5;
    brownNoise.connect(brownGain);

    var lfo = context.createOscillator();
    lfo.frequency.value = 0.1;
    var lfoGain = context.createGain();
    lfoGain.gain.value = 0.1;

    lfo.start(0);
    lfo.connect(lfoGain);
    lfoGain.connect(brownGain.gain);
    return brownGain;

  };
