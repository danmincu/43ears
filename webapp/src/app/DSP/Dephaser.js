
export function createDephaser(context) {
  var bufferSize = 4096;
  var node = context.createScriptProcessor(bufferSize, 1, 1);

  node.power = 0;
  node.previous = [];
  node.onaudioprocess = function (e) {
    //console.log(e.inputBuffer.numberOfChannels)
    var input = e.inputBuffer.getChannelData(0);
    var phase = node.power;

    var output = e.outputBuffer.getChannelData(0);
    for (var i = 0; i < bufferSize; i++) {

      if (i < phase) {
        if (node.previous.length == bufferSize) {
          output[i] = node.previous[bufferSize - phase + i];
        }
      }
      else
        output[i] = input[i - phase];
      output[i] *= 0.91;  
    }

    node.previous = input;
  }
  return node;
};