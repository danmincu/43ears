
export function createStereoficator(context) {
  var bufferSize = 4096 * 2;
  var node = context.createScriptProcessor(bufferSize, 1, 2);
  // this initializing to mono/ no stereofication
  node.power = 0;
  node.previous = [];
  node.onaudioprocess = function (e) {

   // console.log(e.outputBuffer.numberOfChannels);
    var input = e.inputBuffer.getChannelData(0);
    var output = e.outputBuffer.getChannelData(0);
    for (var i = 0; i < bufferSize; i++) {
      output[i] = input[i];// 0.06 *( Math.random() * 2 - 1);
    }

    var phase = node.power;

    var output1 = e.outputBuffer.getChannelData(1);
    for (var i = 0; i < bufferSize; i++) {

      if (i < phase) {
        if (node.previous.length == bufferSize) {
          output1[i] = node.previous[bufferSize - phase + i];
        }
      }
      else
        output1[i] = output[i - phase];
    }

    node.previous = output;
  }
  return node;
};