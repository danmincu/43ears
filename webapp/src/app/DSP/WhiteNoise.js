export function createWhiteNoise(context) {
  // create a fixed 2 second of white noise buffer
  var whiteNoiseBufferSize = 2 * context.sampleRate;
  var noiseBuffer = context.createBuffer(1, whiteNoiseBufferSize, context.sampleRate);
  var output = noiseBuffer.getChannelData(0);
  for (var i = 0; i < whiteNoiseBufferSize; i++) {
    output[i] = Math.random() * 2 - 1;
    output[i] *= 0.55 * output[i];
  }

  var whiteNoise = context.createBufferSource();
  whiteNoise.buffer = noiseBuffer;
  whiteNoise.loop = true;
  whiteNoise.start(0);
  return whiteNoise;
};

export function createStereoDephaseWhiteNoise(context) {
  // create a fixed 2 second of white noise buffer
  var whiteNoiseBufferSize = 2 * context.sampleRate;
  var noiseBuffer = context.createBuffer(2, whiteNoiseBufferSize, context.sampleRate);
  var output = noiseBuffer.getChannelData(0);
  for (var i = 0; i < whiteNoiseBufferSize; i++) {
    output[i] = Math.random() * 2 - 1;
    output[i] *= 0.55 * output[i];
  }

   var stereoDephase = 1000;
   var output1 = noiseBuffer.getChannelData(1);
   for (var i = 0; i < whiteNoiseBufferSize; i++) {
        if (i < stereoDephase)
          output1[i] = output[whiteNoiseBufferSize - stereoDephase + i];
        else
            output1[i] = output[i - stereoDephase];
        // var white = Math.random() * 2 - 1;
        // output1[i] = (lastOut + (0.02 * white)) / 1.02;
        // lastOut = output1[i];
        // output1[i] *= 3.5; 

  }

  var whiteNoise = context.createBufferSource();  
  whiteNoise.buffer = noiseBuffer;
  whiteNoise.loop = true;
  whiteNoise.start(0);
  return whiteNoise;
};