// processor generates continuosly
export function createWhiteNoiseProc(context) {
  var bufferSize = 4096; 
  var node = context.createScriptProcessor(bufferSize, 1, 1);
  node.volume = .55;
  node.onaudioprocess = function (e) {
    var output = e.outputBuffer.getChannelData(0);
    for (var i = 0; i < bufferSize; i++) {
      output[i] = Math.random() * 2 - 1;
      output[i] = node.volume * output[i];
    }    
  }
  return node;
};

var previous = [];

// processor generates continuosly
export function createWhiteNoiseStereoDephaseProc(context) {
  var bufferSize = 4096; 
  var node = context.createScriptProcessor(bufferSize, 1, 2);
  node.phase = 1000;
  node.onaudioprocess = function (e) {
    var output = e.outputBuffer.getChannelData(0);
    for (var i = 0; i < bufferSize; i++) {
      output[i] = Math.random() * 2 - 1;
      output[i] = .45 * output[i];
    }

    var phase = node.phase;
    var output1 = e.outputBuffer.getChannelData(1);
    for (var i = 0; i < bufferSize; i++) {
      
      if (i < phase)
        {
          if (previous.length == bufferSize)
          {
            output1[i] = previous[bufferSize - phase + i];
          }
        }
        else
            output1[i] = output[i - phase];
    }

    previous = output;    
  }
  return node;
};
