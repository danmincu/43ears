import { Component, OnInit } from '@angular/core';
import { loadBuffer } from './DSP/BufferLoader.js';

// import 'src/app/app.component.css';

import $ from 'jquery';

import { createBrownNoise } from './DSP/BrownNoise.js';
import { createOceanBrownNoise } from './DSP/BrownNoise.js';
import { createPinkNoise } from './DSP/PinkNoise.js';
import { createSawPinkNoise } from './DSP/PinkNoise.js';
import { createWhiteNoise } from './DSP/WhiteNoise.js';
import { createWhiteNoiseProc } from './DSP/WhiteNoiseProc.js';
import { createStereoDephaseWhiteNoise } from './DSP/WhiteNoise.js';
import { createWhiteNoiseStereoDephaseProc } from './DSP/WhiteNoiseProc.js';
import { createStereoficator } from './DSP/Stereoficator.js';
import { createDephaser } from './DSP/Dephaser.js';

import * as data from './Data/data.json';
import noises from './Data/noises.json';
import fire from './Data/fire.json';
import crickets from './Data/crickets.json';

import { JAudioContext } from './DSP/AudioContext.js';

// import { AudioContext } from 'angular-audio-context';
// import { AudioContextProxy } from 'angular-audio-context/build/es2018/audio-context-proxy';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'webapp';
  audioContext = null;

  names = [];
  colors = [];
  isPlaying = [];
  audioData = [];
  audioGainArray = [];
  buttons = [];


// this data drives the UI and the functionality
// format [DSP func or MP3 Data, Name, sliderColor]
 audioNodes = [
  [function f(context: AudioContext) { return createWhiteNoise(context); }, 'White', 'white', 'stereo'],
  [(context) => createPinkNoise(context), 'Pink', 'pink', 'stereo'],
  [(context) =>  createBrownNoise(context), 'Brown', 'brown', 'stereo'],
  [(context) => createOceanBrownNoise(context), 'Brown Ocean', 'blue'],
  [(context) => createSawPinkNoise(context), 'Pink Modulated', 'red'],
  [noises.violet, 'Violet', 'purple', 'stereo'],
  [noises.motor, 'Motor', 'orange', 'dephaser', 'stereo'],
  [noises.ring, 'Ring', 'gray', 'dephaser', 'stereo'],
  [noises.hiss, 'Hiss', 'navy', 'dephaser', 'stereo'],
  [noises.wistle, 'Wistle', 'darkgrey', 'dephaser', 'stereo'],
  [data.train, 'Train', 'green', 'dephaser', 'stereo'],
  [data.cicadas, 'Cicadas', 'yellow', 'dephaser', 'stereo'],
  [data.water, 'Water', 'blue', 'dephaser', 'stereo'],
  [fire.fire, 'Fire_Mono', 'red', 'dephaser', 'stereo'],
  [fire.fire, 'Fire_Stereo', 'red'],
  [crickets.crickets, 'Crickets', 'orange'],

  ['/assets/Frogs.m4a', 'Frogs', 'green'],
  ['/assets/Birds.m4a', 'Birds', 'red'],
  ['/assets/Crowd.m4a', 'Crowd', 'yellow'],
  ['/assets/Deep_woods.m4a', 'Woods', 'green'],
  ['/assets/Heavy_rain.m4a', 'Rain', 'blue'],
  ['/assets/Highway.m4a', 'Highway', 'gray'],
  ['/assets/Monks.m4a', 'Monks', 'purple'],
  ['/assets/Ocean_waves.m4a', 'Waves', 'navy'],
  ['/assets/Thunder.m4a', 'Thunder', 'pink'],
  ['/assets/Underwater.m4a', 'Underwater', 'blue'],
  ['/assets/Whales.m4a', 'Whales', 'yellow'],
  ['/assets/Wind.m4a', 'Wind', 'white'],
];

  constructor() {
    // this.initializeUI();
}

ngOnInit(): void {
  this.initializeUI();
}

initializeUI() {
  $('#init').click( () => {

    $('#init').css('opacity', 0);
    $('#mainBox').css('opacity', 0.8);
    // fix for IOS
    // const ac = AudioContext || webkitAudioContext;
    this.audioContext = JAudioContext();

    function sliderPowerChangeDelegate(audioNode) {
      return function() {
        audioNode.power = 20 * this.value;
      };
    }

    function sliderChangeDelegate(audioGainArray, index) {
      return function() {
        audioGainArray[index].gain.value = 0.01 * this.value;
      };
    }

    for (const node of this.audioNodes) {
      const gain = this.audioContext.createGain();
      gain.gain.value = 1;
      this.audioGainArray.push(this.audioContext.createGain());
      this.isPlaying.push(false);
      this.names.push(node[1]);
      this.colors.push(node[2]);
    }

    const playNoise = (text, context) => {
      const handler = (e) => {
        const pos = this.names.indexOf(text);
        if (pos > -1) {
          if (this.isPlaying[pos]) {
            this.audioGainArray[pos].disconnect();
            $(e.target).text(this.names[pos]);
          } else {
            this.audioGainArray[pos].connect(context.destination);
            $(e.target).text('Stop ' + this.names[pos]);
            $('#stop').css('opacity', 1);
          }
          this.isPlaying[pos] = !this.isPlaying[pos];
        }

        // turn off the stop all button if nothing is playing
        $('#stop').css('opacity', 0);
        for (const isPlaying of this.isPlaying) {
          if (isPlaying) {
            $('#stop').css('opacity', 1);
          }
        }
      };
      return handler;
    };

    // generate the UI elements
    for (let i = 0; i < this.names.length; i++) {
      const btn = $(`<button class="button" id="${this.names[i].toLowerCase()}" style="margin:10px">${this.names[i]}</button>`);
      this.buttons.push(btn);
      btn.click(playNoise(this.names[i], this.audioContext));
     // $("#mainBox").append(btn);
      let slider = $(`<div class="slidecontainer">
        <label>Volume</label>
        <input
        style="background:${this.colors[i]}"
        type="range"
        min="0"
        max="100"
        value="100"
        class="slider"
        id="${this.names[i].toLowerCase()}Volume">
      </div>`);

      if (this.audioNodes[i][3]) {
        slider = $(`<div class="slidecontainer">
          <label>Volume</label>
          <input
          style="background:${this.colors[i]}"
          type="range"
          min="0"
          max="100"
          value="100"
          class="slider"
          id="${this.names[i].toLowerCase()}Volume">
          <label>${this.audioNodes[i][3]} effect</label>
          <input
          style="background:${this.colors[i]};margin-top:10px"
          type="range"PowerChangeDelegate(dephaser));

          min="0"
          max="100"
          value="0"
          class="slider"
          id="${this.names[i].toLowerCase()}${this.audioNodes[i][3]}">
        </div>`);
      }

      if (this.audioNodes[i][4]) {
        slider = $(`<div class="slidecontainer">
          <label>Volume</label>
          <input
          style="background:${this.colors[i]}"
          type="range"
          min="0"
          max="100"
          value="100"
          class="slider"
          id="${this.names[i].toLowerCase()}Volume">
          <label>${this.audioNodes[i][3]} effect</label>
          <input
          style="background:${this.colors[i]};margin-top:10px"
          type="range"PowerChangeDelegate(dephaser));
          min="0"
          max="100"
          value="0"
          class="slider"
          id="${this.names[i].toLowerCase()}${this.audioNodes[i][3]}">
        </div>
        <label>${this.audioNodes[i][4]} effect</label>
          <input
          style="background:${this.colors[i]};margin-top:10px"
          type="range"PowerChangeDelegate(dephaser));
          min="0"
          max="100"
          value="0"
          class="slider"
          id="${this.names[i].toLowerCase()}${this.audioNodes[i][4]}">
        </div>`);
      }

      slider.children()[1].addEventListener('input', sliderChangeDelegate(this.audioGainArray, i));
      const div = $('<div class=\'effectBox\'></div>');
      div.append(btn);
      div.append(slider);
      $('#mainBox').append(div);
    }

    // stop all button
    $('#stop').click((e) => {
      for (let i = 0; i < this.audioGainArray.length; i++) {
        if (this.isPlaying[i]) {
          this.audioGainArray[i].disconnect();
          this.buttons[i].text(this.names[i]);
          this.isPlaying[i] = !this.isPlaying[i];
        }
      }
      e.currentTarget.style.opacity = 0.0;
    });



    // hook up the DSP sounds
    for (let i = 0; i < this.audioNodes.length; i++) {

      if (typeof this.audioNodes[i][0] === 'function') {

        const source = (this.audioNodes[i][0] as ((context: AudioContext) => any))(this.audioContext);

        // (this.audioContext);
        // audioNode.connect(audioGainArray[i]);
        if (this.audioNodes[i][4]) {

          let effect = null;
          if (this.audioNodes[i][3] === 'dephaser') {
            effect = createDephaser(this.audioContext);
          } else {
            if (this.audioNodes[i][3] === 'stereo') {
              effect = createStereoficator(this.audioContext);
            }
          }

          // find the slider to connect to
          let name = `#${this.names[i].toLowerCase()}${this.audioNodes[i][3]}`;
          $(name).on('input', sliderPowerChangeDelegate(effect));

          source.connect(effect);

          let effect2;
          if (this.audioNodes[i][4] === 'stereo') {
            effect2 = createStereoficator(this.audioContext);
          }

          // find the slider to connect to
          name = `#${this.names[i].toLowerCase()}${this.audioNodes[i][4]}`;
          $(name).on('input', sliderPowerChangeDelegate(effect2));

          effect.connect(effect2);

          effect2.connect(this.audioGainArray[i]);
        } else {
          if (this.audioNodes[i][3]) {

            let effect;

            if (this.audioNodes[i][3] === 'dephaser') {
              effect = createDephaser(this.audioContext);
            } else {
              if (this.audioNodes[i][3] === 'stereo') {
                effect = createStereoficator(this.audioContext);
              }
            }


            // find the slider to connect to
            const name = `#${this.names[i].toLowerCase()}${this.audioNodes[i][3]}`;
            $(name).on('input', sliderPowerChangeDelegate(effect));

            source.connect(effect);
            effect.connect(this.audioGainArray[i]);
          } else {
            source.connect(this.audioGainArray[i]);
          }
        }
      } else {
        this.audioData.push(this.audioNodes[i][0]);
      }
    }

    // load the data
    loadBuffer(this.audioContext, this.audioData, finishedLoading, this);

    // when finished loading hook up the controls
    function finishedLoading(appComponent: AppComponent, bufferList) {

      for (let i = 0; i < appComponent.audioData.length; i++) {
        const source = appComponent.audioContext.createBufferSource();
        source.buffer = bufferList[i];
        source.start(0);
        source.loop = true;
        const offset = appComponent.audioNodes.length - appComponent.audioData.length;
        const j = i + offset;
        appComponent.audioGainArray[j].gain.value = 1;

        if (appComponent.audioNodes[j][4]) {

          let effect;
          if (appComponent.audioNodes[j][3] === 'dephaser') {
            effect = createDephaser(appComponent.audioContext);
          }

          // find the slider to connect to
          let name = `#${appComponent.names[j].toLowerCase()}${appComponent.audioNodes[j][3]}`;
          $(name).on('input', sliderPowerChangeDelegate(effect));

          source.connect(effect);

          let effect2;
          if (appComponent.audioNodes[j][4] === 'stereo') {
            effect2 = createStereoficator(appComponent.audioContext);
          }
          // find the slider to connect to
          name = `#${appComponent.names[j].toLowerCase()}${appComponent.audioNodes[j][4]}`;
          $(name).on('input', sliderPowerChangeDelegate(effect2));

          effect.connect(effect2);

          effect2.connect(appComponent.audioGainArray[j]);
        } else {
          if (appComponent.audioNodes[j][3]) {
            let effect;
            effect = createStereoficator(appComponent.audioContext);

            // find the slider to connect to
            const name = `#${appComponent.names[j].toLowerCase()}${appComponent.audioNodes[j][3]}`;
            $(name).on('input', sliderPowerChangeDelegate(effect));

            source.connect(effect);
            effect.connect(appComponent.audioGainArray[j]);
          } else {
            source.connect(appComponent.audioGainArray[j]);
          }
        }
      }
    }





  });
}

}
